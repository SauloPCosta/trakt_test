//
//  MovieCell.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMovieCover: UIImageView!
    @IBOutlet weak var lblMovieReleaseYear: UILabel!
    @IBOutlet weak var lblMovieTitle: UILabel!
    
    func setupCellWithMovie(movie: Movie) {
        imgMovieCover.setImageWithUrl(NSURL(string: movie.cover)!)
        lblMovieReleaseYear.text = String(movie.year)
        lblMovieTitle.text = movie.title
    }
}
