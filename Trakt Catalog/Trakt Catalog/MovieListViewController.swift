//
//  MovieListViewController.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    @IBOutlet weak var viewEmptyState: UIView!
    
    @IBOutlet weak var constraintTopSearchBar: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var movieList = [Movie]()
    var filteredMovieList = [Movie]()
    let cellIdentifier = "MovieCell"
    var messageToPresentWhenFinishedLoad = ""
    var page = 1
    var searchOpen = false
    var isFavoriteList = false
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //we can only present allertcontroller after the view finish load
        if messageToPresentWhenFinishedLoad.characters.count > 0 {
            self.presentAllertWithMessage(messageToPresentWhenFinishedLoad)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueMovieDetail" {
            let vc = segue.destinationViewController as! MovieDetailViewController
            let movie = sender as! Movie
            vc.movie = movie
            vc.isFromFavorite = isFavoriteList
        }
    }
    
    
    // MARK: - CollectionView DataSource/Delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredMovieList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! MovieCell
        
        let movie = filteredMovieList[indexPath.row]
        
        cell.setupCellWithMovie(movie)
        
        return cell
    }
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
    {
        var cellAmmount = CGFloat(3)
        let componentsHeight = CGFloat(50)
        
        if Utils.isIpad {
            cellAmmount = CGFloat(6)
        }
        let width = (collectionView.frame.size.width/cellAmmount)-10 //10 is from cell spacing
        
        let height = width*1.5+componentsHeight
        return CGSize.init(width: width, height: height)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let movie = filteredMovieList[indexPath.row]
        performSegueWithIdentifier("SegueMovieDetail", sender: movie)
    }
    
    // MARK: - Actions
    
    @IBAction func showOrHideSearchBar(sender: AnyObject) {
        
        if searchOpen {
            constraintTopSearchBar.constant = -44 //height of a searchbar
            searchBar.resignFirstResponder()
            searchOpen = false
        }
        else {
            constraintTopSearchBar.constant = 0
            searchBar.becomeFirstResponder()
            searchOpen = true
        }
        
        UIView.animateWithDuration(0.5) { 
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - SearchBar delegate

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if !isFavoriteList {
            searchBar.resignFirstResponder()
            
            activityIndicator.startAnimating()
            self.view.userInteractionEnabled = false
            
            BOMovie.searchMovieWithTerm(searchBar.text!) { (movieList, success, error) in
                
                self.activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
                
                if success {
                    self.filteredMovieList = movieList!
                    self.collectionView.reloadData()
                    
                }
                else if error != nil  {
                    
                    if error?.code == Errors.Conection.code {
                        self.presentAllertWithMessage(NSLocalizedString("There is no internet connection", comment: ""))
                    }
                    else {
                        self.presentAllertWithMessage(NSLocalizedString("An error occured while processing your request\nCode: ", comment: "")+String(error?.code))
                    }
                }
                
                self.checkForEmptyList()
            }
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        filteredMovieList = movieList
        collectionView.reloadData()
        searchBar.text = ""
        showOrHideSearchBar(NSNull)
        hideEmptyView()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if isFavoriteList {
            if searchText.characters.count > 0 {
                filteredMovieList = movieList.filter(){$0.title.containsString(searchText)}
            }
            else {
                filteredMovieList = movieList
            }
            
            collectionView.reloadData()
        }
    }

    // MARK: - Methods
    
    func presentAllertWithMessage(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        
        self.presentViewController(alertController, animated: true) {
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
            
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                alertController.dismissViewControllerAnimated(true, completion: nil)

            }
        }
    }
    
    func setupUI() {
        
        if isFavoriteList {
            searchBar.showsCancelButton = false
            navigationItem.title = NSLocalizedString("Favorites", comment: "")
            NSNotificationCenter.defaultCenter().addObserver(
                self,
                selector: #selector(updateFavoriteList),
                name: Notifications.UpdateFavoriteList,
                object: nil)
            
            movieList = BOMovie.getFavorites()
            filteredMovieList = movieList
            
            if movieList.count == 0 {
                presentEmptyViewWithMessage(NSLocalizedString("You have not added any movies to you favorite list yet.", comment: ""))
            }
        }
        else {
            
            refreshControl.tintColor = Colors.kRedTrakt
            refreshControl.addTarget(self, action: #selector(refreshCollectionView), forControlEvents: .ValueChanged)
            collectionView.addSubview(refreshControl)
            collectionView.alwaysBounceVertical = true
            
            activityIndicator.startAnimating()
            
            BOMovie.getMovieListInPage(page) { (movieList, success, error) in
                
                self.activityIndicator.stopAnimating()
                
                if success {
                    self.movieList = movieList!
                    self.filteredMovieList = movieList!
                    self.collectionView.reloadData()
                    self.page += 1
                }
                else if error != nil  {
                    
                    if error?.code == Errors.Conection.code {
                        self.messageToPresentWhenFinishedLoad = NSLocalizedString("There is no internet connection", comment: "")
                    }
                    else {
                        self.messageToPresentWhenFinishedLoad =  NSLocalizedString("An error occured while processing your request\nCode: ", comment: "")+String(error!.code)
                    }
                }
                
                self.checkForEmptyList()
            }
        }
    }
    
    func checkForEmptyList() {
        if self.filteredMovieList.count == 0 {
            self.presentEmptyViewWithMessage(NSLocalizedString("No items found ", comment: ""))
        }
    }
    
    func refreshCollectionView() {
        self.page = 1
        
        BOMovie.getMovieListInPage(page) { (movieList, success, error) in
            
            self.refreshControl.endRefreshing()
            
            if success {
                self.movieList = movieList!
                self.filteredMovieList = movieList!
                self.collectionView.reloadData()
                
            }
            else  if error != nil {
                if error?.code == Errors.Conection.code {
                    self.presentAllertWithMessage(NSLocalizedString("There is no internet connection", comment: ""))
                }
                else {
                    self.presentAllertWithMessage(NSLocalizedString("An error occured while processing your request\nCode: ", comment: "")+String(error?.code))
                }
            }
            
            self.checkForEmptyList()
        }
    }
    
    func presentEmptyViewWithMessage(message: String) {
        lblEmptyMessage.text = message
        UIView.animateWithDuration(0.3) { 
            self.viewEmptyState.alpha = 1
        }
    }
    
    func hideEmptyView() {
        lblEmptyMessage.text = ""
        UIView.animateWithDuration(0.3) {
            self.viewEmptyState.alpha = 0
        }
    }
    
    // MARK: - Notification
    
    func updateFavoriteList(notification: NSNotification){
        movieList = BOMovie.getFavorites()
        filteredMovieList = movieList
        collectionView.reloadData()
        if movieList.count == 0 {
            presentEmptyViewWithMessage(NSLocalizedString("You have not added any movies to you favorite list yet.", comment: ""))
        }
    }

}
