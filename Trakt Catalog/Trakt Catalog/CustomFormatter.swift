//
//  CustomFormatter.swift
//
//  Created by Saulo Costa on 19/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class CustomFormatter: NSDateFormatter {
    
    override init() {
        super.init()
        self.timeZone = NSTimeZone.localTimeZone()
        self.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        self.dateFormat = "yyyy-MM-dd"
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

}
