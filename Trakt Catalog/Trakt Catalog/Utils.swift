//
//  Utils.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit
import Kingfisher

class Utils: NSObject {

    class func convertDataToDictionary(data: NSData) -> [String:AnyObject]? {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    class func convertDataToArray(data: NSData) -> [AnyObject]? {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [AnyObject]
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    class var isIpad:Bool {
        return UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Pad
    }
    class var isIphone:Bool {
        return UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Phone
    }
    
}

extension UIImageView  {
    func setImageWithUrl(url:NSURL) -> Void {
        self.kf_setImageWithURL(url, placeholderImage: UIImage(named: "img_moviePlaceholder"))
    }
}

extension UIViewController {
    public var isVisible: Bool {
        if isViewLoaded() {
            return view.window != nil
        }
        return false
    }
    
    public var isTopViewController: Bool {
        if self.navigationController != nil {
            return self.navigationController?.visibleViewController === self
        } else if self.tabBarController != nil {
            return self.tabBarController?.selectedViewController == self && self.presentedViewController == nil
        } else {
            return self.presentedViewController == nil && self.isVisible
        }
    }
}