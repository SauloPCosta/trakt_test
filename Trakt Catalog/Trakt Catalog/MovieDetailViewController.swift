//
//  MovieDetailViewController.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 23/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit
import Social

class MovieDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblRuntime: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTagline: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    // MARK: - Properties
    var movie = Movie()
    var isFromFavorite = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueZoomImage" {
            let vc = segue.destinationViewController as! ImageZoomViewController
            let imageURL = sender as! String
            vc.selectedImageUrl = imageURL
        }
    }
 
    
    // MARK: - CollectionView DataSource/Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movie.images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GaleryCell", forIndexPath: indexPath) as! GaleryCell
        
        let imageURL = movie.images[indexPath.row]
        
        cell.imgGalery.setImageWithUrl(NSURL(string: imageURL.urlString)!)
        
        return cell
    }
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
    {
        return CGSize(width: 150, height: 150)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let imageURL = movie.images[indexPath.row].urlString
        performSegueWithIdentifier("SegueZoomImage", sender: imageURL)
    }
    
    // MARK: - Actions
    
    @IBAction func dismiss(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func zoomImageCover(sender: AnyObject) {
        performSegueWithIdentifier("SegueZoomImage", sender: movie.cover)
    }
    
    @IBAction func toggleFavorite(sender: UIButton) {
        if BOMovie.isFavoriteMovie(movie) {

            let alertMessage = NSLocalizedString("Do you want to remove the movie from your favorite list?", comment: "")
            
            
            let alertController = UIAlertController(title: NSLocalizedString("Attention", comment: ""), message: alertMessage, preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { (action) in
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .Default) { (action) in
                sender.setImage(UIImage(named: "ic_favorite"), forState: UIControlState.Normal)
                BOMovie.removeMovieFromFavorite(self.movie)
                
                if self.isFromFavorite {
                    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.UpdateFavoriteList, object: nil)
                    self.dismiss(NSNull)
                }
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                
            }
        }
        else {
            sender.setImage(UIImage(named: "ic_favorite_filled"), forState: UIControlState.Normal)
            BOMovie.saveMovieInFavorite(movie)
        }
        
    }
    
    @IBAction func shareMovie(sender: AnyObject) {
        //since the new facebook privacy policy, we need to show the facebook share option out UIActivityViewController
        
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("Where do you want to share this movie?", comment: ""), preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let facebookAction = UIAlertAction(title: "Facebook", style: .Default) { (action) in
            let composeSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            composeSheet.addImage(self.imgCover.image)
            
            self.presentViewController(composeSheet, animated: true, completion: nil)
        }
        alertController.addAction(facebookAction)
        
        let twitterAction = UIAlertAction(title: "Twitter", style: .Default) { (action) in
            let composeSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            composeSheet.addImage(self.imgCover.image)
            
            self.presentViewController(composeSheet, animated: true, completion: nil)
        }
        alertController.addAction(twitterAction)
        
        self.presentViewController(alertController, animated: true) {}
    }
    
    
    // MARK: - Methods
    
    func setupUI() {
        lblTitle.text = movie.title
        lblTagline.text = movie.tagline
        lblGenre.text = "Genres:" + movie.genres
        lblRuntime.text = "Runtime: " + String(movie.runtime)
        lblRating.text = String(format: "Rating: %0.2f", movie.rating)
        lblOverview.text = movie.overview
        imgCover.setImageWithUrl(NSURL(string: movie.cover)!)
        btnShare.layer.borderColor = UIColor.whiteColor().CGColor
        
        if BOMovie.isFavoriteMovie(movie) {
            btnFavorite.setImage(UIImage(named: "ic_favorite_filled"), forState: UIControlState.Normal)
        }
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        lblReleaseDate.text = "Released: " + formatter.stringFromDate(movie.released)
    }

}
