//
//  API.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit
import RealmSwift

class DAOMovie: NSObject {
    
    class func getMovieListInPage(page:Int, completion: (movieList:[Movie]?,success:Bool, error:NSError?) -> Void) {
        let  endPoint = "movies/popular"
        
        var params = [String:AnyObject]()
        params["page"] = page
        params["limit"] = Paging.NumberOfItensPerPage
        params["extended"] = "images,full"
        
        RequestHelper.sharedInstance.request(.GET, urlString: endPoint, requestParameters: params, encoding: .URL) { (httpResponse, responseData, responseError) in
            if responseError == nil {
                if let response = responseData {
                    
                    let responseArray = Utils.convertDataToArray(response)
                    if responseArray != nil {
                        var movies = [Movie]()
                        
                        for item in responseArray! {
                            let movieObj = Movie()
                            movieObj.parseFromDict(item as! [String : AnyObject])
                            movies.append(movieObj)
                        }
                        completion(movieList: movies, success: true, error: nil)
                    }
                    else {
                        completion(movieList: nil, success: false, error: nil)
                    }
                }
                else {
                    completion(movieList: nil, success: false, error: nil)
                }
            }
            else {
                completion(movieList: nil, success: false, error: responseError)
            }
        }
    }
    
    class func searchMovieWithTerm(term:String, completion: (movieList:[Movie]?,success:Bool, error:NSError?) -> Void) {
        let  endPoint = "search/movie?"
        
        var params = [String:AnyObject]()
        params["extended"] = "images,full"
        params["limit"] = Paging.NumberOfItensPerPage
        params["query"] = term
        
        RequestHelper.sharedInstance.request(.GET, urlString: endPoint, requestParameters: params, encoding: .URL) { (httpResponse, responseData, responseError) in
            if responseError == nil {
                if let response = responseData {
                    
                    let responseArray = Utils.convertDataToArray(response)
                    if responseArray != nil {
                        var movies = [Movie]()
                        
                        for item in responseArray! {
                            if let movieDict = item["movie"] as? [String : AnyObject] {
                                let movieObj = Movie()
                                movieObj.parseFromDict(movieDict)
                                movies.append(movieObj)
                            }
                        }
                        completion(movieList: movies, success: true, error: nil)
                    }
                    else {
                        completion(movieList: nil, success: false, error: nil)
                    }
                }
                else {
                    completion(movieList: nil, success: false, error: nil)
                }
            }
            else {
                completion(movieList: nil, success: false, error: responseError)
            }
        }
    }
    
    class func removeMoviesFromFavorite(favoriteMovies: [Movie]) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(favoriteMovies)
        }
    }
    
    class func saveMovieInFavorite(movie: Movie) {
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(movie, update: false)
        }
    }
    
    class func getFavorites() -> [Movie]{
        let realm = try! Realm()
        
        let favoriteMovies = realm.objects(Movie.self)
        
        //we should return the movie list in array format, not realm Results
        var movieArray = [Movie]()
        
        for movie in favoriteMovies {
            movieArray.append(movie)
        }
        
        
        return movieArray
    }

}
