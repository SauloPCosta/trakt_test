//
//  GaleryCell.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 24/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class GaleryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgGalery: UIImageView!
}
