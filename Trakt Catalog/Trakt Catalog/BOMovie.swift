//
//  BOMovie.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 26/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class BOMovie: NSObject {

    class func getMovieListInPage(page:Int, completion: (movieList:[Movie]?,success:Bool, error:NSError?) -> Void) {
        //ensure there is internet connection
        if !NetworkHelper.connectedToNetwork() {
            completion(movieList: nil, success: false, error: Errors.Conection)
        }
        
        DAOMovie.getMovieListInPage(page) { (movieList, success, error) in
            completion(movieList: movieList, success: success, error: error)
        }
    }
    
    class func searchMovieWithTerm(term:String, completion: (movieList:[Movie]?,success:Bool, error:NSError?) -> Void) {
        //ensure there is internet connection
        if !NetworkHelper.connectedToNetwork() {
            completion(movieList: nil, success: false, error: Errors.Conection)
        }
        
        DAOMovie.searchMovieWithTerm(term) { (movieList, success, error) in
            completion(movieList: movieList, success: success, error: error)
        }
    }
    
    class func isFavoriteMovie(movie: Movie) -> Bool {
        
        var favoriteMovies = self.getFavorites()
        
        //ensure the object exist in database
        favoriteMovies = favoriteMovies.filter(){$0.idMovie == movie.idMovie}
        
        if favoriteMovies.count > 0 {
            return true
        }
        else {
            return false
        }
    }
    
    class func getFavorites() -> [Movie]{
        
        let favoriteMovies = DAOMovie.getFavorites()
        
        return favoriteMovies
    }
    
    class func removeMovieFromFavorite(movie: Movie) {
        
        let favoriteMovie = DAOMovie.getFavorites().filter{$0.idMovie == movie.idMovie}
        
        DAOMovie.removeMoviesFromFavorite(favoriteMovie)
    }
    
    class func saveMovieInFavorite(movie: Movie) {
        //we should create another movie object so we dont loose the reference in controllers
        let movieToSave = Movie()
        
        movieToSave.idMovie = movie.idMovie
        movieToSave.title = movie.title
        movieToSave.released = movie.released
        movieToSave.year = movie.year
        movieToSave.tagline = movie.tagline
        movieToSave.overview = movie.overview
        movieToSave.rating = movie.rating
        movieToSave.genres = movie.genres
        movieToSave.cover = movie.cover
        movieToSave.runtime = movie.runtime
        
        for image in movie.images {
            movieToSave.images.append(image)
        }
        
        DAOMovie.saveMovieInFavorite(movieToSave)
    }
    
}
