//
//  Movie.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit
import RealmSwift

class Movie: Object {
    dynamic var idMovie = 0
    dynamic var title = ""
    dynamic var released = NSDate()
    dynamic var year = 0
    dynamic var tagline = ""
    dynamic var overview = ""
    dynamic var rating: Float = 0.0
    dynamic var genres = ""
    dynamic var cover = ""
    dynamic var runtime = 0
    let images = List<MovieURL>()
    
    internal func parseFromDict(dict: [String:AnyObject]) {
        
        if let titleUnwrap = dict["title"] as? String {
            title = titleUnwrap
        }
        if let releasedUnwrap = dict["released"] as? String {
            let formatter = CustomFormatter()
            released = formatter.dateFromString(releasedUnwrap)!
        }
        if let yearUnwrap = dict["year"] as? Int {
            year = yearUnwrap
        }
        if let runtimeUnwrap = dict["runtime"] as? Int {
            runtime = runtimeUnwrap
        }
        if let taglineUnwrap = dict["tagline"] as? String {
            tagline = taglineUnwrap
        }
        if let overviewUnwrap = dict["overview"] as? String {
            overview = overviewUnwrap
        }
        if let ratingUnwrap = dict["rating"] as? Float {
            rating = ratingUnwrap
        }
        if let genresUnwrap = dict["genres"] as? [String] {
            for genre in genresUnwrap {
                genres = genres + ", " +  genre
            }
            genres = String(genres.characters.dropFirst())
        }
        if let idsUnwrap = dict["ids"] as? [String:AnyObject] {
            if let idDictUnwrap = idsUnwrap["trakt"] as? Int {
                idMovie = idDictUnwrap
            }
        }
        if let imagesUnwrap = dict["images"] as? [String:AnyObject] {
            if let fanartUnwrap = imagesUnwrap["fanart"] as? [String:AnyObject] {
                if let mediumUnwrap = fanartUnwrap["medium"] as? String {
                    let url = MovieURL()
                    url.urlString = mediumUnwrap
                    images.append(url)
                }
            }
            if let posterUnwrap = imagesUnwrap["poster"] as? [String:AnyObject] {
                if let mediumUnwrap = posterUnwrap["medium"] as? String {
                    let url = MovieURL()
                    url.urlString = mediumUnwrap
                    images.append(url)
                    cover = mediumUnwrap
                }
            }
            if let logoUnwrap = imagesUnwrap["logo"] as? [String:AnyObject] {
                if let mediumUnwrap = logoUnwrap["medium"] as? String {
                    let url = MovieURL()
                    url.urlString = mediumUnwrap
                    images.append(url)
                }
            }
            if let clearartUnwrap = imagesUnwrap["clearart"] as? [String:AnyObject] {
                if let mediumUnwrap = clearartUnwrap["full"] as? String {
                    let url = MovieURL()
                    url.urlString = mediumUnwrap
                    images.append(url)
                }
            }
            if let bannerUnwrap = imagesUnwrap["banner"] as? [String:AnyObject] {
                if let mediumUnwrap = bannerUnwrap["full"] as? String {
                    let url = MovieURL()
                    url.urlString = mediumUnwrap
                    images.append(url)
                }
            }
        }
    }
    
    override static func primaryKey() -> String? {
        return "idMovie"
    }
}

class MovieURL: Object {
    dynamic var urlString = ""
}