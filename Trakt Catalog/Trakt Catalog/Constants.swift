//
//  Constants.swift
//
//  Created by Saulo Costa on 18/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class Constants: NSObject {

}

struct Colors {
    static let kRedTrakt = UIColor.init(red: 0.922, green: 0.129, blue: 0.180, alpha: 1)
}

struct Paging {
    static let NumberOfItensPerPage = 30
}

enum Notifications
{
    static let UpdateFavoriteList = "UpdateFavoriteList"
}

enum Errors {
    static let Conection = NSError(domain: "NoConnection", code: -99, userInfo: [NSLocalizedDescriptionKey: "No Internet connection"])
}