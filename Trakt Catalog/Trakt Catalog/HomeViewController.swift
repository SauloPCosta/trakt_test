//
//  HomeViewController.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 25/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var btnSearchMovies: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueMovieList" {
            let isFavorite = sender as! Bool
            let vc = segue.destinationViewController as! MovieListViewController
            vc.isFavoriteList = isFavorite
        }
    }

    // MARK: - Actions
    
    @IBAction func presentMovieList(sender: UIButton) {
        self.performSegueWithIdentifier("SegueMovieList", sender: sender.tag)
    }
    
    // MARK: - Methods 
    
    func setupUI() {
        btnFavorites.layer.borderColor = UIColor.whiteColor().CGColor
        btnSearchMovies.layer.borderColor = UIColor.whiteColor().CGColor
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
}
