//
//  RequestHelper.swift
//  Trakt Catalog
//
//  Created by Saulo Costa on 21/08/16.
//  Copyright © 2016 Saulo Costa. All rights reserved.
//

import UIKit
import Alamofire

class RequestHelper: NSObject {
    
    static let sharedInstance = RequestHelper()
    
    var alamofireManager : Alamofire.Manager?
    
    override init() {
        //Set timeout in AlamofireManager
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 20// seconds
        configuration.timeoutIntervalForResource = 20
        
        alamofireManager = Alamofire.Manager(configuration: configuration)
    }
    
    func request (requestType: Alamofire.Method, urlString: String ,requestParameters: [String:AnyObject], encoding:ParameterEncoding, completion: (httpResponse: NSHTTPURLResponse?,responseData:NSData?, responseError:NSError?) -> Void) {
        
        var baseUrl = ""
        var headers = [String:String]()
        
        if let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist"), dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            baseUrl = (dict["API-URL"] as! String)
            headers["Content-type"] = "application/json"
            headers["trakt-api-version"] = "2"
            headers["trakt-api-key"] = dict["TraktClientSecret"] as? String
        }
        
        let urlString = String (format: "%@%@", baseUrl,urlString)
        
        alamofireManager!.request(requestType, urlString, parameters: requestParameters,encoding: encoding, headers: headers)
            .response {
                (request, response, data, error) in
                completion(httpResponse: response, responseData: data, responseError: error)
                
        }
    }

}
